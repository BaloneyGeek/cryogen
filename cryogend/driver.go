package main

import (
	"log"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
)

/* GPIO Relay Connection Pinout Chart
 *     CONNECTION  BOARD BCM
 *   Water Sensor      7   4
 *       Fan High     13  27
 *     Fan Medium     15  22
 *        Fan Low     16  23
 *    Swing Motor     18  24
 *     Water Pump     36  16
 *
 * IMPORTANT: HIGH is OFF, LOW is ON
 */

const (
	PinInWaterSensor uint8 = 4
	PinOutFanHigh    uint8 = 27
	PinOutFanMedium  uint8 = 22
	PinOutFanLow     uint8 = 23
	PinOutAirSwing   uint8 = 24
	PinOutWaterPump  uint8 = 16
)

type FanState string

const (
	Off    FanState = "off"
	Low    FanState = "low"
	Medium FanState = "medium"
	High   FanState = "high"
)

type CoolerDriver interface {
	SenseWaterAvailable() bool

	GetFanState() FanState
	SetFanState(FanState)
	GetSwingState() bool
	SetSwingState(bool)
	GetPumpState() bool
	SetPumpState(bool)

	Destroy()
}

type DriverImpl struct {
	mWatchdogChannel chan struct{}

	PinInWaterSensor rpio.Pin
	PinOutFanHigh    rpio.Pin
	PinOutFanMedium  rpio.Pin
	PinOutFanLow     rpio.Pin
	PinOutAirSwing   rpio.Pin
	PinOutWaterPump  rpio.Pin
}

func (this *DriverImpl) SenseWaterAvailable() bool {
	return this.PinInWaterSensor.Read() == rpio.High
}

func (this *DriverImpl) GetFanState() FanState {
	if this.PinOutFanHigh.Read() == rpio.Low {
		return High
	} else if this.PinOutFanMedium.Read() == rpio.Low {
		return Medium
	} else if this.PinOutFanLow.Read() == rpio.Low {
		return Low
	} else {
		return Off
	}
}

func (this *DriverImpl) SetFanState(theFanState FanState) {
	if theFanState == this.GetFanState() {
		log.Println("Fan state unchanged: requested state is the same as current state")
		return
	}

	log.Println("Fan state change requested. Turning off all fan speeds first")
	this.PinOutFanHigh.High()
	this.PinOutFanMedium.High()
	this.PinOutFanLow.High()

	switch theFanState {
	case Low:
		this.PinOutFanLow.Low()
		log.Println("Fan state set to LOW")
	case Medium:
		this.PinOutFanMedium.Low()
		log.Println("Fan state set to MEDIUM")
	case High:
		this.PinOutFanHigh.Low()
		log.Println("Fan state set to HIGH")
	case Off:
		log.Println("Fan state set to OFF")
	default:
		log.Println("Invalid fan state. Fan is OFF")
	}
}

func (this *DriverImpl) GetSwingState() bool {
	return this.PinOutAirSwing.Read() == rpio.Low
}

func (this *DriverImpl) SetSwingState(theSwingState bool) {
	if theSwingState == this.GetSwingState() {
		log.Println("Swing motor state unchanged: requested state is the same as current state")
		return
	}

	if theSwingState == true {
		this.PinOutAirSwing.Low()
		log.Println("Swing motor turned ON")
	} else {
		this.PinOutAirSwing.High()
		log.Println("Swing motor turned OFF")
	}
}

func (this *DriverImpl) GetPumpState() bool {
	return this.PinOutWaterPump.Read() == rpio.Low
}

func (this *DriverImpl) SetPumpState(thePumpState bool) {
	if thePumpState == this.GetPumpState() {
		log.Println("Water pump state unchanged: requested state is the same as current state")
		return
	}

	if thePumpState == true {
		this.PinOutWaterPump.Low()
		log.Println("Water pump turned ON")
	} else {
		this.PinOutWaterPump.High()
		log.Println("Water pump turned OFF")
	}
}

func (this *DriverImpl) ResetAll() {
	this.PinInWaterSensor.Input()
	this.PinInWaterSensor.PullDown()
	this.PinOutFanHigh.Output()
	this.PinOutFanHigh.High()
	this.PinOutFanMedium.Output()
	this.PinOutFanMedium.High()
	this.PinOutFanLow.Output()
	this.PinOutFanLow.High()
	this.PinOutAirSwing.Output()
	this.PinOutAirSwing.High()
	this.PinOutWaterPump.Output()
	this.PinOutWaterPump.High()
}

func (this *DriverImpl) Destroy() {
	log.Println("Signalling the pump dry run watchdog to stop and quit")
	close(this.mWatchdogChannel)

	log.Println("Resetting the GPIO state and closing the device gracefully")
	this.ResetAll()
	rpio.Close()

	log.Println("Done")
}

func NewCoolerDriver() CoolerDriver {
	lErr := rpio.Open()
	if lErr != nil {
		log.Fatalln(lErr)
	}

	lDriverImpl := &DriverImpl{
		mWatchdogChannel: make(chan struct{}),

		PinInWaterSensor: rpio.Pin(PinInWaterSensor),
		PinOutFanHigh:    rpio.Pin(PinOutFanHigh),
		PinOutFanMedium:  rpio.Pin(PinOutFanMedium),
		PinOutFanLow:     rpio.Pin(PinOutFanLow),
		PinOutAirSwing:   rpio.Pin(PinOutAirSwing),
		PinOutWaterPump:  rpio.Pin(PinOutWaterPump),
	}
	lDriverImpl.ResetAll()

	// Start a watchdog so that if the water tank runs dry while the pump
	// is running, then the pump is shut down
	go func() {
		log.Println("Started pump dry run watchdog")

		for {
			time.Sleep(1 * time.Second)
			select {
			case <-lDriverImpl.mWatchdogChannel:
				log.Println("Closing the pump dry run watchdog")
				return
			default:
				if !lDriverImpl.SenseWaterAvailable() && lDriverImpl.GetPumpState() {
					lDriverImpl.SetPumpState(false)
					log.Println("Watchdog turned off pump because the water tank ran dry")
				}
			}
		}
	}()

	return lDriverImpl
}
