package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/brutella/hc"
	"github.com/gorilla/mux"
)

func main() {
	log.Println("Cryogend starting up...")
	lDriver := NewCoolerDriver()
	defer lDriver.Destroy()

	// Find our HomeKit pairing pin
	lPin := os.Getenv("CRYOGEN_HAP_PIN")
	if lPin == "" {
		lPin = "98761234"
	}
	log.Println("Apple HomeKit pairing pin is " + lPin)

	// Initialize our HomeKit Accessory
	lAcc := CreateHAPAccessory(lDriver)
	lHAPConfig := hc.Config{
		Pin: lPin,
	}

	// Create the HomeKit transport first, because of the way it does error handling
	lTransport, lErr := hc.NewIPTransport(lHAPConfig, lAcc.Accessory)
	if lErr != nil {
		log.Fatal(lErr)
	}

	// Create the API service for our web apps
	lRouter := mux.NewRouter().StrictSlash(true)
	lAPISubrouter := lRouter.PathPrefix("/api").Subrouter()
	CreateAPIService(lDriver, lAPISubrouter)

	// Handle the webui static files
	lDataPath := os.Getenv("CRYOGEN_DATA_PATH")
	if lDataPath == "" {
		lDataPath = "/opt/cryogen"
	}
	lStaticServer := http.FileServer(http.Dir(lDataPath + "/webui"))
	lRouter.PathPrefix("/").Handler(lStaticServer)

	// Find our API service port
	lPort := os.Getenv("CRYOGEN_WEB_PORT")
	if lPort == "" {
		lPort = "8080"
	}
	log.Println("Starting API service on port " + lPort)

	// It's good to set timeouts to prevent denial of service attacks
	lServer := &http.Server{
		Addr:         "0.0.0.0:" + lPort,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      lRouter,
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		log.Println("API service is now listening")
		if lErr := lServer.ListenAndServe(); lErr != nil {
			log.Println(lErr)
		}
	}()

	// Start the HAP transport, again in a non-blocking goroutine.
	go func() {
		lTransport.Start()
	}()

	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// and SIGTERM (systemd). SIGKILL or SIGQUIT will not be caught.
	lIntChan := make(chan os.Signal, 1)
	signal.Notify(lIntChan, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-lIntChan

	// Stop the HAP transport first
	<-lTransport.Stop()

	// Create a deadline to wait for.
	lContext, lCancel := context.WithTimeout(context.Background(), time.Second*15)
	defer lCancel()

	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	lServer.Shutdown(lContext)
	log.Println("Shutting down")
}
