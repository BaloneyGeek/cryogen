package main

import (
	"log"

	"github.com/brutella/hc/accessory"
	"github.com/brutella/hc/characteristic"
	"github.com/brutella/hc/service"
)

type CryogenHAPService struct {
	*service.Service
	mDriver CoolerDriver

	Name                        *characteristic.Name
	Active                      *characteristic.Active
	CurrentHeaterCoolerState    *characteristic.CurrentHeaterCoolerState
	TargetHeaterCoolerState     *characteristic.TargetHeaterCoolerState
	CurrentTemperature          *characteristic.CurrentTemperature
	CoolingThresholdTemperature *characteristic.CoolingThresholdTemperature
	RotationSpeed               *characteristic.RotationSpeed
	SwingMode                   *characteristic.SwingMode
}

type CryogenHAPAccessory struct {
	*accessory.Accessory
	Cryogen *CryogenHAPService
}

func NewCryogenHAPService(theDriver CoolerDriver) *CryogenHAPService {
	const lTypeID = "BC"
	lSvc := CryogenHAPService{}
	lSvc.mDriver = theDriver
	lSvc.Service = service.New(lTypeID)

	lSvc.Name = characteristic.NewName()
	lSvc.Name.SetValue("Cryogen Air Cooler - Block 2")
	lSvc.AddCharacteristic(lSvc.Name.Characteristic)

	lSvc.Active = characteristic.NewActive()
	lSvc.Active.OnValueRemoteGet(func() int {
		if lSvc.mDriver.GetFanState() != Off {
			return characteristic.ActiveActive
		}
		return characteristic.ActiveInactive
	})
	lSvc.Active.OnValueRemoteUpdate(func(theValue int) {
		log.Println("HAP Request: Set Active to", theValue)
		if theValue == characteristic.ActiveInactive {
			lSvc.mDriver.SetFanState(Off)
			lSvc.mDriver.SetPumpState(false)
			lSvc.mDriver.SetSwingState(false)
		} else {
			if lSvc.mDriver.GetFanState() == Off {
				lSvc.mDriver.SetFanState(High)
			}

		}
	})
	lSvc.AddCharacteristic(lSvc.Active.Characteristic)

	lSvc.CurrentHeaterCoolerState = characteristic.NewCurrentHeaterCoolerState()
	lSvc.CurrentHeaterCoolerState.OnValueRemoteGet(func() int {
		log.Println("HAP Request: Get CurrentHeaterCoolerState")
		if lSvc.mDriver.GetPumpState() {
			return characteristic.CurrentHeaterCoolerStateCooling
		} else if lSvc.mDriver.GetFanState() != Off {
			return characteristic.CurrentHeaterCoolerStateIdle
		} else {
			return characteristic.CurrentHeaterCoolerStateInactive
		}
	})
	lSvc.AddCharacteristic(lSvc.CurrentHeaterCoolerState.Characteristic)

	lSvc.TargetHeaterCoolerState = characteristic.NewTargetHeaterCoolerState()
	lSvc.TargetHeaterCoolerState.SetMinValue(0)
	lSvc.TargetHeaterCoolerState.SetMaxValue(2)
	lSvc.TargetHeaterCoolerState.SetStepValue(2)
	lSvc.TargetHeaterCoolerState.OnValueRemoteGet(func() int {
		log.Println("HAP Request: Get TargetHeaterCoolerState")
		if lSvc.mDriver.GetPumpState() {
			return characteristic.TargetHeaterCoolerStateCool
		}
		return characteristic.TargetHeaterCoolerStateAuto
	})
	lSvc.TargetHeaterCoolerState.OnValueRemoteUpdate(func(theValue int) {
		log.Println("HAP Request: Set TargetHeaterCoolerState to", theValue)
		if !lSvc.mDriver.SenseWaterAvailable() {
			return
		}
		lState := theValue == characteristic.TargetHeaterCoolerStateCool
		lSvc.mDriver.SetPumpState(lState)
	})
	lSvc.AddCharacteristic(lSvc.TargetHeaterCoolerState.Characteristic)

	lSvc.CurrentTemperature = characteristic.NewCurrentTemperature()
	lSvc.CurrentTemperature.SetValue(30)
	lSvc.AddCharacteristic(lSvc.CurrentTemperature.Characteristic)

	lSvc.CoolingThresholdTemperature = characteristic.NewCoolingThresholdTemperature()
	lSvc.CoolingThresholdTemperature.SetValue(20)
	lSvc.AddCharacteristic(lSvc.CoolingThresholdTemperature.Characteristic)

	lSvc.RotationSpeed = characteristic.NewRotationSpeed()
	lSvc.RotationSpeed.SetMinValue(0)
	lSvc.RotationSpeed.SetMaxValue(3)
	lSvc.RotationSpeed.SetStepValue(1)
	lSvc.RotationSpeed.OnValueRemoteGet(func() float64 {
		lFanState := lSvc.mDriver.GetFanState()
		log.Println("HAP Request: Get RotationSpeed")
		switch lFanState {
		case Low:
			return 1
		case Medium:
			return 2
		case High:
			return 3
		case Off:
			fallthrough
		default:
			return 0
		}
	})
	lSvc.RotationSpeed.OnValueRemoteUpdate(func(theValue float64) {
		lValue := uint64(theValue)
		log.Println("HAP Request: Set RotationSpeed to", lValue)
		switch lValue {
		case 1:
			lSvc.mDriver.SetFanState(Low)
		case 2:
			lSvc.mDriver.SetFanState(Medium)
		case 3:
			lSvc.mDriver.SetFanState(High)
		case 0:
			fallthrough
		default:
			lSvc.mDriver.SetFanState(Off)
		}
	})
	lSvc.AddCharacteristic(lSvc.RotationSpeed.Characteristic)

	lSvc.SwingMode = characteristic.NewSwingMode()
	lSvc.SwingMode.OnValueRemoteGet(func() int {
		log.Println("HAP Request: Get SwingMode")
		if lSvc.mDriver.GetSwingState() {
			return 1
		}
		return 0
	})
	lSvc.SwingMode.OnValueRemoteUpdate(func(theValue int) {
		log.Println("HAP Request: Set SwingMode to", theValue)
		lValue := theValue == 1
		lSvc.mDriver.SetSwingState(lValue)
	})
	lSvc.AddCharacteristic(lSvc.SwingMode.Characteristic)

	return &lSvc
}

func CreateHAPAccessory(theDriver CoolerDriver) *CryogenHAPAccessory {
	lInfo := accessory.Info{
		Name:             "Cooler",
		Manufacturer:     "Boudhayan Gupta",
		Model:            "Cryogen Air Cooler",
		SerialNumber:     "HDBW94-0706BG",
		FirmwareRevision: "Block 2 (20200611.01)",
	}

	lAcc := CryogenHAPAccessory{}
	lAcc.Accessory = accessory.New(lInfo, accessory.TypeAirConditioner)
	lAcc.Cryogen = NewCryogenHAPService(theDriver)
	lAcc.AddService(lAcc.Cryogen.Service)

	return &lAcc
}
