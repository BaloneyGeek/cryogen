package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

type RequestError struct {
	Status  int
	Message string
}

func (this *RequestError) Error() string {
	return this.Message
}

type Element struct {
	Name           string   `json:"name"`
	Description    string   `json:"description"`
	ValueType      string   `json:"value_type"`
	Value          *Variant `json:"value"`
	ValuesAccepted []string `json:"values_accepted,omitempty"`
}

type ElementCollection struct {
	Sensors  *[]*Element `json:"sensors,omitempty"`
	Controls *[]*Element `json:"controls,omitempty"`
}

type ContextKey int

const (
	KeyRequestValue ContextKey = iota
)

type RequestValue struct {
	Value *Variant `json:"value"`
}

func WriteElement(theElement *Element, theWriter io.Writer) {
	json.NewEncoder(theWriter).Encode(theElement)
}

func WriteElements(theSensors *[]*Element, theControls *[]*Element, theWriter io.Writer) {
	lElCollection := ElementCollection{
		Sensors:  theSensors,
		Controls: theControls,
	}
	json.NewEncoder(theWriter).Encode(lElCollection)
}

func ParseRequestJson(theResponse http.ResponseWriter, theRequest *http.Request, theDest *RequestValue) error {
	theRequest.Body = http.MaxBytesReader(theResponse, theRequest.Body, 1048576)
	lDecoder := json.NewDecoder(theRequest.Body)
	lDecoder.DisallowUnknownFields()

	lErr := lDecoder.Decode(theDest)
	if lErr != nil {
		var lSyntaxError *json.SyntaxError
		var lTypeError *json.UnmarshalTypeError

		switch {
		case errors.As(lErr, &lSyntaxError):
			lMsg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", lSyntaxError.Offset)
			return &RequestError{Status: http.StatusBadRequest, Message: lMsg}
		case errors.Is(lErr, io.ErrUnexpectedEOF):
			lMsg := fmt.Sprintf("Request body contains badly-formed JSON")
			return &RequestError{Status: http.StatusBadRequest, Message: lMsg}
		case errors.As(lErr, &lTypeError):
			lMsg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", lTypeError.Field, lTypeError.Offset)
			return &RequestError{Status: http.StatusBadRequest, Message: lMsg}
		case strings.HasPrefix(lErr.Error(), "json: unknown field "):
			lFieldName := strings.TrimPrefix(lErr.Error(), "json: unknown field ")
			lMsg := fmt.Sprintf("Request body contains unknown field %s", lFieldName)
			return &RequestError{Status: http.StatusBadRequest, Message: lMsg}
		case errors.Is(lErr, io.EOF):
			lMsg := "Request body must not be empty"
			return &RequestError{Status: http.StatusBadRequest, Message: lMsg}
		case lErr.Error() == "http: request body too large":
			lMsg := "Request body must not be larger than 1MB"
			return &RequestError{Status: http.StatusRequestEntityTooLarge, Message: lMsg}
		default:
			return lErr
		}
	}

	lErr = lDecoder.Decode(&struct{}{})
	if lErr != io.EOF {
		lMsg := "Request body must only contain a single JSON object"
		return &RequestError{Status: http.StatusBadRequest, Message: lMsg}
	}
	return nil
}

type CryogenService struct {
	mDriver CoolerDriver
}

func (this *CryogenService) SenseWaterAvailable() *Element {
	return &Element{
		Name:        "water_available",
		Description: "Indicate if there is any water available in the water tank",
		ValueType:   "bool",
		Value:       &Variant{Boolean: this.mDriver.SenseWaterAvailable()},
	}
}

func (this *CryogenService) GetFanState() *Element {
	return &Element{
		Name:           "main_fan",
		Description:    "Set the speed of the main fan",
		ValueType:      "enum",
		Value:          &Variant{String: string(this.mDriver.GetFanState())},
		ValuesAccepted: []string{"off", "low", "medium", "high"},
	}
}

func (this *CryogenService) GetSwingState() *Element {
	return &Element{
		Name:        "air_swing",
		Description: "Enable or disable the air swing motor",
		ValueType:   "bool",
		Value:       &Variant{Boolean: this.mDriver.GetSwingState()},
	}
}

func (this *CryogenService) GetPumpState() *Element {
	return &Element{
		Name:        "water_pump",
		Description: "Enable or disable the evaporative cooling water pump",
		ValueType:   "bool",
		Value:       &Variant{Boolean: this.mDriver.GetPumpState()},
	}
}

func (this *CryogenService) GetAllSensors() *[]*Element {
	return &[]*Element{
		this.SenseWaterAvailable(),
	}
}

func (this *CryogenService) GetAllControls() *[]*Element {
	return &[]*Element{
		this.GetFanState(),
		this.GetSwingState(),
		this.GetPumpState(),
	}
}

func (this *CryogenService) HandleSenseWaterAvailable(theResponse http.ResponseWriter, theRequest *http.Request) {
	theResponse.Header().Set("Content-Type", "application/json")
	theResponse.WriteHeader(http.StatusOK)
	if theRequest.Method == http.MethodOptions {
		return
	}
	WriteElement(this.SenseWaterAvailable(), theResponse)
}

func (this *CryogenService) HandleGetSensors(theResponse http.ResponseWriter, theRequest *http.Request) {
	theResponse.Header().Set("Content-Type", "application/json")
	theResponse.WriteHeader(http.StatusOK)
	if theRequest.Method == http.MethodOptions {
		return
	}
	WriteElements(this.GetAllSensors(), nil, theResponse)
}

func (this *CryogenService) HandleGetFanState(theResponse http.ResponseWriter, theRequest *http.Request) {
	theResponse.Header().Set("Content-Type", "application/json")
	theResponse.WriteHeader(http.StatusOK)
	if theRequest.Method == http.MethodOptions {
		return
	}
	WriteElement(this.GetFanState(), theResponse)
}

func (this *CryogenService) HandleSetFanState(theResponse http.ResponseWriter, theRequest *http.Request) {
	lReqValue := theRequest.Context().Value(KeyRequestValue).(*Variant)

	lCurrentState := this.mDriver.GetFanState()
	lNewState := FanState(strings.ToLower(lReqValue.String))
	if lNewState == lCurrentState {
		this.HandleGetFanState(theResponse, theRequest)
		return
	}

	switch lNewState {
	case Off:
		fallthrough
	case Low:
		fallthrough
	case Medium:
		fallthrough
	case High:
		this.mDriver.SetFanState(lNewState)
		this.HandleGetFanState(theResponse, theRequest)
	default:
		lErrStr := fmt.Sprintf("Invalid fan state requested (%s). Please refer to values_accepted for acceptable states", lNewState)
		http.Error(theResponse, lErrStr, http.StatusBadRequest)
	}
}

func (this *CryogenService) HandleGetSwingState(theResponse http.ResponseWriter, theRequest *http.Request) {
	theResponse.Header().Set("Content-Type", "application/json")
	theResponse.WriteHeader(http.StatusOK)
	if theRequest.Method == http.MethodOptions {
		return
	}
	WriteElement(this.GetSwingState(), theResponse)
}

func (this *CryogenService) HandleSetSwingState(theResponse http.ResponseWriter, theRequest *http.Request) {
	lReqValue := theRequest.Context().Value(KeyRequestValue).(*Variant)
	if lReqValue.String != "" {
		http.Error(theResponse, "Expected boolean for key \"value\", got string", http.StatusBadRequest)
		return
	}
	this.mDriver.SetSwingState(lReqValue.Boolean)
	this.HandleGetSwingState(theResponse, theRequest)
}

func (this *CryogenService) HandleGetPumpState(theResponse http.ResponseWriter, theRequest *http.Request) {
	theResponse.Header().Set("Content-Type", "application/json")
	theResponse.WriteHeader(http.StatusOK)
	if theRequest.Method == http.MethodOptions {
		return
	}
	WriteElement(this.GetPumpState(), theResponse)
}

func (this *CryogenService) HandleSetPumpState(theResponse http.ResponseWriter, theRequest *http.Request) {
	lReqValue := theRequest.Context().Value(KeyRequestValue).(*Variant)
	if lReqValue.String != "" {
		http.Error(theResponse, "Expected boolean for key \"value\", got string", http.StatusBadRequest)
		return
	}
	if lReqValue.Boolean == true && this.mDriver.SenseWaterAvailable() == false {
		http.Error(theResponse, "Cannot start the water pump as the water tank is empty. Please refill the water tank", http.StatusConflict)
		return
	}
	this.mDriver.SetPumpState(lReqValue.Boolean)
	this.HandleGetPumpState(theResponse, theRequest)
}

func (this *CryogenService) HandleGetControls(theResponse http.ResponseWriter, theRequest *http.Request) {
	theResponse.Header().Set("Content-Type", "application/json")
	theResponse.WriteHeader(http.StatusOK)
	if theRequest.Method == http.MethodOptions {
		return
	}
	WriteElements(nil, this.GetAllControls(), theResponse)
}

func (this *CryogenService) HandleRoot(theResponse http.ResponseWriter, theRequest *http.Request) {
	theResponse.Header().Set("Content-Type", "application/json")
	theResponse.WriteHeader(http.StatusOK)
	if theRequest.Method == http.MethodOptions {
		return
	}
	WriteElements(this.GetAllSensors(), this.GetAllControls(), theResponse)
}

func ValidationMiddleware(theNextHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(theResponse http.ResponseWriter, theRequest *http.Request) {
		theResponse.Header().Set("Access-Control-Allow-Origin", "*")
		theResponse.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Accept")
		theResponse.Header().Set("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS")
		if theRequest.Method == http.MethodPost {
			lContentType := strings.ToLower(theRequest.Header.Get("Content-Type"))
			if !strings.HasPrefix(lContentType, "application/json") {
				http.Error(theResponse, "This API only supports requests encoded as JSON.", http.StatusUnsupportedMediaType)
				return
			}

			var lRequestValue RequestValue
			lErr := ParseRequestJson(theResponse, theRequest, &lRequestValue)
			if lErr != nil {
				var lReqErr *RequestError
				if errors.As(lErr, &lReqErr) {
					http.Error(theResponse, lReqErr.Message, lReqErr.Status)
				} else {
					log.Println(lErr.Error())
					http.Error(theResponse, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				return
			}

			if lRequestValue.Value == nil {
				http.Error(theResponse, "Request body does not contain required field \"value\"", http.StatusBadRequest)
				return
			}

			lContext := context.WithValue(theRequest.Context(), KeyRequestValue, lRequestValue.Value)
			theRequest = theRequest.WithContext(lContext)
		}
		theNextHandler.ServeHTTP(theResponse, theRequest)
	})
}

func CreateAPIService(theDriver CoolerDriver, theRouter *mux.Router) {
	lAPIService := &CryogenService{theDriver}
	theRouter.HandleFunc("/sensors/water_available", lAPIService.HandleSenseWaterAvailable).Methods(http.MethodGet, http.MethodOptions)
	theRouter.HandleFunc("/sensors", lAPIService.HandleGetSensors).Methods(http.MethodGet, http.MethodOptions)
	theRouter.HandleFunc("/controls/main_fan", lAPIService.HandleGetFanState).Methods(http.MethodGet, http.MethodOptions)
	theRouter.HandleFunc("/controls/main_fan", lAPIService.HandleSetFanState).Methods(http.MethodPost)
	theRouter.HandleFunc("/controls/air_swing", lAPIService.HandleGetSwingState).Methods(http.MethodGet, http.MethodOptions)
	theRouter.HandleFunc("/controls/air_swing", lAPIService.HandleSetSwingState).Methods(http.MethodPost)
	theRouter.HandleFunc("/controls/water_pump", lAPIService.HandleGetPumpState).Methods(http.MethodGet, http.MethodOptions)
	theRouter.HandleFunc("/controls/water_pump", lAPIService.HandleSetPumpState).Methods(http.MethodPost)
	theRouter.HandleFunc("/controls", lAPIService.HandleGetControls).Methods(http.MethodGet, http.MethodOptions)
	theRouter.HandleFunc("/", lAPIService.HandleRoot).Methods(http.MethodGet, http.MethodOptions)
	theRouter.Use(ValidationMiddleware)
}
