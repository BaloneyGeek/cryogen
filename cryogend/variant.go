package main

import (
	"fmt"
	"strconv"
)

type Variant struct {
	String  string
	Boolean bool
}

func (this *Variant) MarshalJSON() ([]byte, error) {
	if this.String != "" {
		return []byte(fmt.Sprintf(`"%s"`, this.String)), nil
	}
	return []byte(strconv.FormatBool(this.Boolean)), nil
}

func (this *Variant) UnmarshalJSON(theData []byte) error {
	if len(theData) == 0 {
		return nil
	}
	if theData[0] == '"' {
		this.String = string(theData[1 : len(theData)-1])
		return nil
	}
	lBool, lErr := strconv.ParseBool(string(theData))
	if lErr != nil {
		return lErr
	}
	this.Boolean = lBool
	return nil
}
