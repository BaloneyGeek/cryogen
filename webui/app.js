var oVueApp = new Vue({
    el: "#cryo-app-main",

    data: {
        sApiHost: "http://" + window.location.hostname,
        iApiPort: window.location.port,
        oAxiosClient: null,

        sStateFan: "off",
        bStateSwing: false,
        bStatePump: false,
        bSensorWaterAvailable: false
    },

    computed: {
        sStateFanComputed: {
            get: function() {
                return this.sStateFan;
            },
            set: function(sNewValue) {
                this.sendFanStateRequest(sNewValue);
            }
        },
        sStateSwingComputed: {
            get: function() {
                if (this.bStateSwing) {
                    return "on";
                }
                return "off";
            },
            set: function(sNewValue) {
                this.sendSwingStateRequest(sNewValue);
            }
        },
        sStatePumpComputed: {
            get: function () {
                if (this.bStatePump) {
                    return "on";
                }
                return "off";
            },
            set: function (sNewValue) {
                this.sendPumpStateRequest(sNewValue);
            }
        }
    },

    methods: {
        getRemoteState: function() {
            this.oAxiosClient.get("/").then(oResponse => {
                oResponse.data.controls.forEach(oControl => {
                    switch(oControl.name) {
                    case "main_fan":
                        this.sStateFan = oControl.value;
                        break;
                    case "air_swing":
                        this.bStateSwing = oControl.value;
                        break;
                    case "water_pump":
                        this.bStatePump = oControl.value;
                        break;
                    };
                });
                oResponse.data.sensors.forEach(oSensor => {
                    switch(oSensor.name) {
                    case "water_available":
                        this.bSensorWaterAvailable = oSensor.value;
                        break; 
                    };
                });
            });
        },
        sendFanStateRequest: function(sRequestedState) {
            this.oAxiosClient.post("/controls/main_fan", { value: sRequestedState }).then(oResponse => {
                this.sStateFan = oResponse.data.value;
            });
        },
        sendSwingStateRequest: function(sRequestedState) {
            let bRequestedState = (sRequestedState === "on");
            this.oAxiosClient.post("/controls/air_swing", { value: bRequestedState }).then(oResponse => {
                this.bStateSwing = oResponse.data.value;
            });
        },
        sendPumpStateRequest: function (sRequestedState) {
            let bRequestedState = (sRequestedState === "on");
            this.oAxiosClient.post("/controls/water_pump", { value: bRequestedState }).then(oResponse => {
                this.bStatePump = oResponse.data.value;
            }).catch(oError => {
                console.log(oError.response);
            });
        }
    },

    mounted: function() {
        this.oAxiosClient = axios.create({
            baseURL: this.sApiHost + ":" + this.iApiPort + "/api"
        });
        this.getRemoteState();
    }
});
