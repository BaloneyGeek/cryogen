module cryogend

go 1.14

require (
	github.com/brutella/hc v1.2.2
	github.com/gorilla/mux v1.7.4
	github.com/stianeikeland/go-rpio/v4 v4.4.0
)
